from __future__ import with_statement
import time

import boto3
from fabric import operations
from fabric.api import run
from fabric.tasks import execute

def create_instance():
    client = boto3.client('ec2')
    response = client.run_instances(
        ImageId='ami-fce3c696', # ubuntu 14.04
        MinCount=1,
        MaxCount=1,
        KeyName='int-dev',
        SecurityGroupIds=['sg-328a1449',],
        InstanceType='t2.micro',
    )
    instance_id = response['Instances'][0]['InstanceId']
    return instance_id

def wait_for_instance_to_boot(instance_id):
    client = boto3.client('ec2')
    while True:
        time.sleep(5)
        response = client.describe_instance_status(
            InstanceIds=[instance_id],
            IncludeAllInstances=True,
        )
        status = response['InstanceStatuses'][0]['InstanceState']['Name']
        if status == 'running':
            print "Instance is now running."
            break
        print "Instance still pending, sleeping..."    

def tag_instance(instance_id, name):
    client = boto3.client('ec2')
    client.create_tags(
        Resources=[instance_id],
        Tags=[{
            'Key': 'Name',
            'Value': name
        }]
    )
    
instance_id = "i-0488cb9cc8ed8a9bd" # create_instance()
print "Created instance %s" % (instance_id,)
#wait_for_instance_to_boot(instance_id)
#tag_instance(instance_id, "buildbot-%s" % (instance_id))

ec2 = boto3.resource('ec2')
instance = ec2.Instance(instance_id)
instance_name = instance.public_dns_name
print instance_name

def upload_files(a):
    print a
    operations.put("buildbot_setup", "~/")

fab_env = {
    'hosts': [instance_name],
    'user': 'ubuntu',
    'key_filename': 'int-dev.pem',
}

execute(upload_files, **fab_env)

