from __future__ import with_statement
import time

import boto3

def user_data(version):
    return """#!/bin/bash
mkdir -p /opt/eventsuite
cd /opt/eventsuite
wget https://s3.amazonaws.com/ironninja-es-builds/eventsuite.%s.tar.gz
tar -zxf eventsuite.%s.tar.gz
HOME=/opt/eventsuite ./bin/eventsuite start
""" % (version, version)

def create_instance(security_group_ids, user_data):
    client = boto3.client('ec2')
    response = client.run_instances(
        # ImageId='ami-fce3c696', # ubuntu 14.04
        ImageId='ami-ddf13fb0', # ubuntu 16.04
        MinCount=1,
        MaxCount=1,
        KeyName='int-dev',
        SecurityGroupIds=security_group_ids,
        InstanceType='t2.micro',
        UserData=user_data,
    )
    instance_id = response['Instances'][0]['InstanceId']
    return instance_id

def wait_for_instance_to_boot(instance_id):
    client = boto3.client('ec2')
    while True:
        time.sleep(5)
        response = client.describe_instance_status(
            InstanceIds=[instance_id],
            IncludeAllInstances=True,
        )
        status = response['InstanceStatuses'][0]['InstanceState']['Name']
        if status == 'running':
            print "Instance is now running."
            break
        print "Instance still pending, sleeping..."    

def tag_instance(instance_id, tags):
    client = boto3.client('ec2')
    client.create_tags(
        Resources=[instance_id],
        Tags=tags
    )

def create_dev_instance(version):
    instance_id = create_instance(
        ["sg-c0c05ebb", "sg-b9f864c2"], # security groups
        user_data(version)
    )

    ec2 = boto3.resource('ec2')
    instance = ec2.Instance(instance_id)

    print "Creating instance %s" % (instance_id,)
    #instance.wait_until_running()
    wait_for_instance_to_boot(instance_id)

    name = "dev.%s@%s" % (version, instance.private_ip_address)
    tags = [
        {
            'Key': 'Name',
            'Value': name
        },
        {
        'Key': 'Role',
            'Value': 'dev-server'
        }
    ]
    tag_instance(instance_id, tags)

    print instance.private_ip_address
    print "ubuntu@%s" % (instance.public_dns_name)
