import sys, time

import boto3
from fabric.api import *

from aws_create_instance import create_dev_instance

env.key_filename = 'int-dev.pem'

def dev_box(version="0.0.1"):
    create_dev_instance(version)

def builder():
    env.user = 'ubuntu'
    env.hosts = ['ec2-54-152-205-234.compute-1.amazonaws.com']

def migrate():
    print "Need to figure this out"

def build(version=None,env="dev01",migrate=False,delete=True):
    if not version:
        sys.exit("Please provide a version ( func:key=value )")
    print "Building release %s" % (version)
    folder = "tag_%s" % (version.replace('.', '_'))
    run("rm -rf %s" % (folder))
    run("mkdir %s" % (folder))
    with cd(folder):
        run("git clone git@bitbucket.org:ironninjatech/eventsuite.git .")
        run("git checkout %s" % (version))
        run(" ".join([
            "aws s3 cp",
            "s3://ironninja-secrets/%s.secrets.exs" % (env),
            "config/prod.secret.exs"
        ]))
        run("mix deps.get")
        run("npm install --production")
        #run("npm install babel-preset-es2015 --save")
        run("brunch build")
        run("MIX_ENV=prod APP_VERSION=%s mix fullbuild" % (version))
        if migrate:
            run("MIX_ENV=prod APP_VERSION=%s mix ecto.migrate")
        run(" ".join([
            "aws s3 cp",
            "rel/eventsuite/releases/%s/eventsuite.tar.gz" % (version),
            "s3://ironninja-es-builds/eventsuite.%s.tar.gz" % (version)
        ]))
    if delete:
        run("rm -rf %s" % (folder))

def create_buildbot():
    put("buildbot_setup")
    put("buildbot_user_setup")
    put("sshconfig", "~/.ssh/config")
    put("~/.ssh/id_bbbb_rsa", "~/.ssh/")
    run("chmod 600 ~/.ssh/id_bbbb_rsa")

    sudo("source buildbot_setup")
    run("source buildbot_user_setup")
    run("git clone git@bitbucket.org:ironninjatech/eventsuite.git")

def s3():
    env.user = 'ubuntu'
    run("mkdir -p ~/.aws/")
    put("buildbot_aws_config", "~/.aws/config")
    sudo("apt-get update")
    sudo("apt-get install -y awscli")
    run("aws s3 ls")

def node():
    # the direct download of a 'curl script | sudo bash' cycle, because
    # I don't trust direct script setups on remote production boxes
    # also, needed because ubuntu is waaaaay behind on node/npm packages
    put("nodesetup_6.x")
    sudo("source nodesetup_6.x")
    sudo("apt-get install -y nodejs")
    sudo("npm install -g brunch")
    with cd("eventsuite"):
        run("npm install")

        # Uh...yeah; explicit install fixes this issue:
        # error: Compiling of web/static/js/app.js failed. Couldn't find preset "/home/ubuntu/eventsuite/node_modules/babel-preset-es2015
        # via: https://github.com/laravel/elixir/issues/354
        # And yes, not the same elixir...still works :P
        run("npm install babel-preset-es2015 --save")

        run("brunch build")
